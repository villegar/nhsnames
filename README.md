# NHSnames

NHSnames is a small R project to format NHS organisation names. 

The package will allow a NHS Organisation name provided as ALL CAPS to be formatted in title case with NHS remaining in capitals and conjuctions (and, of etc) to be in lower case.

It will also provide abreviations and shortening for use in graph axis etc.


To install use:

```

install.packages("remotes")

remotes::install_gitlab("polc1410/nhsnames")

```

For help see: 

https://polc1410.gitlab.io/nhsnames
